<?php

namespace App\Http\Livewire\Admin;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Producto;

class AdminProductComponent extends Component
{
    use WithPagination;

    //Eliminar Producto
    public function eliminarProducto($id)
    {
        $producto = Producto::find($id);
        $producto->delete();
        session()->flash('message','El producto se ha eliminado exitosamente');
    }

    public function render()
    {
        $productos = Producto::paginate(10);
        return view('livewire.admin.admin-product-component',['productos'=>$productos])->layout('layouts.base');
    }
}
