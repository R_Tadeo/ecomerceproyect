<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Cupon;

class AdminAddCouponComponent extends Component
{
    public $codigo;
    public $tipo;
    public $valor;
    public $cart_valor;

    public function updated($fields)
    {
        $this->validateOnly($fields, [
            'codigo' => 'required|unique:cupons',
            'tipo' => 'required',
            'valor' => 'required|numeric',
            'cart_valor' => 'required|numeric'
        ]);
    }

    #Guardar Cupón
    public function guardarCupon()
    {
        $this->validate([
            'codigo' => 'required|unique:cupons',
            'tipo' => 'required',
            'valor' => 'required|numeric',
            'cart_valor' => 'required|numeric'
        ]);
        $cupon = new Cupon();
        $cupon->codigo = $this->codigo;
        $cupon->tipo = $this->tipo;
        $cupon->valor = $this->valor;
        $cupon->cart_valor = $this->cart_valor;
        $cupon->save();
        session()->flash('message','El cupón se ha creado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-add-coupon-component')->layout('layouts.base');
    }
}
