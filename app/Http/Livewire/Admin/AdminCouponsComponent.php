<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Cupon;

class AdminCouponsComponent extends Component
{

    #Eliminar Cupón
    public function eliminarCupon($cupon_id)
    {
        $cupon = Cupon::find($cupon_id);
        $cupon->delete();
        session()->flash('message','El cupón se ha eliminado exitosamente');
    }

    public function render()
    {
        $cupones = Cupon::all();
        return view('livewire.admin.admin-coupons-component',['cupones'=>$cupones])->layout('layouts.base');
    }
}
