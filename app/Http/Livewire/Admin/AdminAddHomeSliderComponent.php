<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\HomeSlider;
use Livewire\WithFileUploads;

class AdminAddHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $titulo;
    public $subtitulo;
    public $precio;
    public $link;
    public $imagen;
    public $estado;

    public function mount()
    {
        $this->status = 0;
    }

    //Guardar Slider
    public function guardarSlider()
    {
        $slider = new HomeSlider();
        $slider->titulo = $this->titulo;
        $slider->subtitulo = $this->subtitulo;
        $slider->precio = $this->precio;
        $slider->link = $this->link;
        $imagenNombre = Carbon::now()->timestamp. '.' .$this->imagen->extension();
        $this->imagen->storeAs('sliders',$imagenNombre);
        $slider->imagen = $imagenNombre;
        $slider->estado = $this->estado;
        $slider->save();
        session()->flash('message','El slider se ha guardado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-add-home-slider-component')->layout('layouts.base');
    }
}
