<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\HomeSlider;
use Livewire\WithFileUploads;

class AdminEditHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $titulo;
    public $subtitulo;
    public $precio;
    public $link;
    public $imagen;
    public $estado;
    public $nuevaimagen;
    public $slider_id;

    public function mount($slider_id)
    {
        $slider = HomeSlider::find($slider_id);
        $this->titulo = $slider->titulo;
        $this->subtitulo = $slider->subtitulo;
        $this->precio = $slider->precio;
        $this->link = $slider->link;
        $this->imagen = $slider->imagen;
        $this->estado = $slider->estado;
        $this->slider_id = $slider->id;
    }

    //Actualizar información del Slider
    public function actualizarSlider()
    {
        $slider = HomeSlider::find($this->slider_id);
        $slider->titulo = $this->titulo;
        $slider->subtitulo = $this->subtitulo;
        $slider->precio = $this->precio;
        $slider->link = $this->link;
        if ($this->nuevaimagen)
        {
            $imagenNombre = Carbon::now()->timestamp. '.' .$this->nuevaimagen->extension();
            $this->nuevaimagen->storeAs('sliders',$imagenNombre);
            $slider->imagen = $imagenNombre;
        }
        $slider->estado = $this->estado;
        $slider->save();
        session()->flash('message','El slider se ha actualizado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-home-slider-component')->layout('layouts.base');
    }
}
