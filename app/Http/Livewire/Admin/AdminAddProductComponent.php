<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class AdminAddProductComponent extends Component
{
    use WithFileUploads;
    public $nombre;
    public $slug;
    public $descripcion_corta;
    public $descripcion;
    public $precio_regular;
    public $precio_venta;
    public $SKU;
    public $stock_estatus;
    public $caracteristecas;
    public $cantidad;
    public $imagen;
    public $categoria_id;

    public function mount()
    {
        $this->stock_estatus = "En existemcia";
        $this->caracteristecas = 0;
    }

    //Aunto completa el slug
    public function generarSlug()
    {
        $this->slug = Str::slug($this->nombre.'-');
    }

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'nombre' => 'required',
            'slug' => 'required|unique:productos',
            'descripcion_corta' => 'required',
            'descripcion' => 'required',
            'precio_regular' => 'required|numeric',
            'precio_venta' => 'numeric',
            'SKU' => 'required',
            'stock_estatus' => 'required',
            'cantidad' => 'required|numeric',
            'imagen' => 'required|mimes:jpeg,jpg,png',
            'categoria_id' => 'required'
        ]);
    }
    
    //Guarda el nuevo producto
    public function guardarProducto()
    {
        $this->validate([
            'nombre' => 'required',
            'slug' => 'required|unique:productos',
            'descripcion_corta' => 'required',
            'descripcion' => 'required',
            'precio_regular' => 'required|numeric',
            'precio_venta' => 'numeric',
            'SKU' => 'required',
            'stock_estatus' => 'required',
            'cantidad' => 'required|numeric',
            'imagen' => 'required|mimes:jpeg,jpg,png',
            'categoria_id' => 'required'
        ]);
        $producto = new Producto();
        $producto->nombre = $this->nombre;
        $producto->slug = $this->slug;
        $producto->descripcion_corta = $this->descripcion_corta;
        $producto->descripcion = $this->descripcion;
        $producto->precio_regular = $this->precio_regular;
        $producto->precio_venta = $this->precio_venta;
        $producto->SKU = $this->SKU;
        $producto->stock_estatus = $this->stock_estatus;
        $producto->caracteristecas = $this->caracteristecas;
        $producto->cantidad = $this->cantidad;
        $imagenNombre = Carbon::now()->timestamp. '.' .$this->imagen->extension();
        $this->imagen->storeAs('products',$imagenNombre);
        $producto->imagen = $imagenNombre;
        $producto->categoria_id = $this->categoria_id;
        $producto->save();
        session()->flash('message','El producto se ha guardado exitosamente');
    }

    public function render()
    {
        $categorias = Categoria::all();
        return view('livewire.admin.admin-add-product-component',['categorias'=>$categorias])->layout('layouts.base');
    }
}
