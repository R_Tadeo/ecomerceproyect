<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Categoria;
use Illuminate\Support\Str;

class AdminEditCategoryComponent extends Component
{
    public $categoria_slug;
    public $categoria_id;
    public $nombre;
    public $slug;

    //Recuperación de los datos
    public function mount($categoria_slug){
        $this->categoria_slug = $categoria_slug;
        $categoria = Categoria::where('slug', $categoria_slug)->first();
        $this->categoria_id = $categoria->id;
        $this->nombre = $categoria->nombre;
        $this->slug = $categoria->slug;
    }

    //Aunto completa el slug
    public function generarslug()
    {
        $this->slug = Str::slug($this->nombre);
    }

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'nombre' => 'required',
            'slug' => 'required|unique:categorias'
        ]);
    }

    //Actualizar Categoria
    public function actualizarCategoria()
    {
        $this->validate([
            'nombre' => 'required',
            'slug' => 'required|unique:categorias'
        ]);
        $categoria = Categoria::find($this->categoria_id);
        $categoria->nombre = $this->nombre;
        $categoria->slug = $this->slug;
        $categoria->save();
        session()->flash('message','La categoria se ha actualizado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-category-component')->layout('layouts.base');
    }
}
