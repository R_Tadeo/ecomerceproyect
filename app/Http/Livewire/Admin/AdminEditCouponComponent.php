<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Cupon;

class AdminEditCouponComponent extends Component
{
    public $codigo;
    public $tipo;
    public $valor;
    public $cart_valor;
    public $cupon_id;

    public function mount($cupon_id)
    {
        $cupon = Cupon::find($cupon_id);
        $this->codigo = $cupon->codigo;
        $this->tipo = $cupon->tipo;
        $this->valor = $cupon->valor;
        $this->cart_valor = $cupon->cart_valor;
        $this->cupon_id = $cupon->id;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields, [
            'codigo' => 'required|unique:cupons',
            'tipo' => 'required',
            'valor' => 'required|numeric',
            'cart_valor' => 'required|numeric'
        ]);
    }

    #Guardar Cupón
    public function actualizarCupon()
    {
        $this->validate([
            'codigo' => 'required|unique:cupons',
            'tipo' => 'required',
            'valor' => 'required|numeric',
            'cart_valor' => 'required|numeric'
        ]);
        $cupon = Cupon::find($this->cupon_id);
        $cupon->codigo = $this->codigo;
        $cupon->tipo = $this->tipo;
        $cupon->valor = $this->valor;
        $cupon->cart_valor = $this->cart_valor;
        $cupon->save();
        session()->flash('message','El cupón se ha actualizado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-coupon-component')->layout('layouts.base');
    }
}
