<?php

namespace App\Http\Livewire;

use App\Models\Producto;
use Livewire\Component;
use Cart;

class DetailsComponent extends Component
{
    public $slug;
    public $qty;

    public function mount($slug)
    {
        $this->slug = $slug;
        $this->qty = 1;
    }

    public function store($producto_id,$producto_nombre,$producto_precio)
    {
        Cart::instance('cart')->add($producto_id,$producto_nombre,$this->qty,$producto_precio)->associate('App\Models\Producto');
        session()->flash('success_message','Producto agregado al carrito');
        return redirect()->route('producto.cart');
    }

    public function incrementa()
    {
        $this->qty++;
    }

    public function decrementa(){
        if ($this->qty > 1) {
            $this->qty--;
        }
    }

    public function render()
    {
        $producto = Producto::where('slug', $this->slug)->first();
        $productos_popular = Producto::inRandomOrder()->limit(4)->get();
        $productos_relacionados = Producto::where('categoria_id', $producto->categoria_id)->inRandomOrder()->limit(7)->get();
        return view('livewire.details-component',['producto'=>$producto,'productos_popular'=>$productos_popular,'productos_relacionados'=>$productos_relacionados])->layout('layouts.base');
    }
}
