<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CartCoundComponent extends Component
{
    protected $listeners = ['refreshComponent' => '$refresh'];
    public function render()
    {
        return view('livewire.cart-cound-component');
    }
}
