<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Transaccion;
use App\Models\OrdenItem;
use App\Models\Orden;
use App\Models\Envio;
use Cart;

class CheckoutComponent extends Component
{
    public $ship_to_different;

    public $nombre;
    public $apellido;
    public $telefono;
    public $email;
    public $line1;
    public $line2;
    public $ciudad;
    public $provincia;
    public $pais;
    public $codigopostal;

    public $s_nombre;
    public $s_apellido;
    public $s_telefono;
    public $s_email;
    public $s_line1;
    public $s_line2;
    public $s_ciudad;
    public $s_provincia;
    public $s_pais;
    public $s_codigopostal;

    public $metodoPago;
    public $gracias;

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'nombre' => 'required',
            'apellido' => 'required',
            'telefono' => 'required|numeric',
            'email' => 'required|email',
            'line1' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'pais' => 'required',
            'codigopostal' => 'required',
            'metodoPago' => 'required'
        ]);

        if ($this->ship_to_different)
        {
            $this->validateOnly($fields,[
                's_nombre' => 'required',
                's_apellido' => 'required',
                's_telefono' => 'required|numeric',
                's_email' => 'required|email',
                's_line1' => 'required',
                's_ciudad' => 'required',
                's_provincia' => 'required',
                's_pais' => 'required',
                's_codigopostal' => 'required'
            ]);
        }
    }

    //Realizar Pedido
    public function placeOrder()
    {
        $this->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'telefono' => 'required|numeric',
            'email' => 'required|email',
            'line1' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'pais' => 'required',
            'codigopostal' => 'required',
            'metodoPago' => 'required'
        ]);

        $orden = new Orden();
        $orden->usuario_id = Auth::user()->id;
        $orden->subtotal = session()->get('checkout')['subtotal'];
        $orden->descuento = session()->get('checkout')['descuento'];
        $orden->iva = session()->get('checkout')['iva'];
        $orden->total = session()->get('checkout')['total'];
        $orden->nombre = $this->nombre;
        $orden->apellido = $this->apellido;
        $orden->telefono = $this->telefono;
        $orden->email = $this->email;
        $orden->line1 = $this->line1;
        $orden->line2 = $this->line2;
        $orden->ciudad = $this->ciudad;
        $orden->provincia = $this->provincia;
        $orden->pais = $this->pais;
        $orden->codigopostal = $this->codigopostal;
        $orden->estatus = 'ordenado';
        $orden->envio_diferente = $this->ship_to_different ? 1:0;
        $orden->save();

        foreach(Cart::instance('cart')->content() as $item)
        {
            $ordenItem = new OrdenItem();
            $ordenItem->producto_id = $item->id;
            $ordenItem->orden_id = $orden->id;
            $ordenItem->precio = $item->price;
            $ordenItem->cantidad = $item->qty;
            $ordenItem->save();
        }

        if ($this->ship_to_different)
        {
            $this->validate([
                's_nombre' => 'required',
                's_apellido' => 'required',
                's_telefono' => 'required|numeric',
                's_email' => 'required|email',
                's_line1' => 'required',
                's_ciudad' => 'required',
                's_provincia' => 'required',
                's_pais' => 'required',
                's_codigopostal' => 'required'
            ]);

            $envio = new Envio();
            $envio->orden_id = $orden->id;
            $envio->nombre = $this->s_nombre;
            $envio->apellido = $this->s_apellido;
            $envio->telefono = $this->s_telefono;
            $envio->email = $this->s_email;
            $envio->line1 = $this->s_line1;
            $envio->line2 = $this->s_line2;
            $envio->ciudad = $this->s_ciudad;
            $envio->provincia = $this->s_provincia;
            $envio->pais = $this->s_pais;
            $envio->codigopostal = $this->s_codigopostal;
            $envio->save();
        }

        if ($this->metodoPago == 'cod')
        {
            $transaccion = new Transaccion();
            $transaccion->usuario_id = Auth::user()->id;
            $transaccion->orden_id = $orden->id;
            $transaccion->metodo = 'cod';
            $transaccion->estado = 'pendiente';
            $transaccion->save();
        }

        $this->gracias = 1;
        Cart::instance('cart')->destroy();
        session()->forget('checkout');

    }

    public function varifyForCheckout()
    {
        if (!Auth::check())
        {
            return redirect()->route('login');
        }
        else if($this->gracias)
        {
            return redirect()->route('gracias');
        }
        else if(!session()->get('checkout'))
        {
            return redirect()->route('producto.cart');
        }
    }

    public function render()
    {
        $this->varifyForCheckout();
        return view('livewire.checkout-component')->layout('layouts.base');
    }
}
