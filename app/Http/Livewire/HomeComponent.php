<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\HomeSlider;
use App\Models\HomeCategoria;

class HomeComponent extends Component
{
    public function render()
    {
        $sliders = HomeSlider::where('estado',1)->get();
        $ultimosProductos = Producto::orderBy('created_at','DESC')->get()->take(8);
        $categoria = HomeCategoria::find(1);
        $cats = explode(',',$categoria->sel_categorias);
        $categorias = Categoria::whereIn('id',$cats)->get();
        $no_productos = $categoria->no_productos;
        return view('livewire.home-component', ['sliders'=>$sliders, 'ultimosProductos'=>$ultimosProductos,'categorias'=>$categorias,'no_productos'=>$no_productos])->layout('layouts.base');
    }
}
