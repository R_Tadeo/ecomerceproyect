<?php

namespace App\Http\Livewire;

use App\Models\Producto;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Categoria;
use Cart;

class CategoryComponent extends Component
{
    public $clasificacion;
    public $cantidad;
    public $categoria_slug;

    //Acomodo de la vista de la tienda
    public function mount($categoria_slug)
    {
        $this->clasificacion = "default";
        $this->cantidad = 12;
        $this->categoria_slug = $categoria_slug;
    }

    //Almacena los productos en el carrito
    public function store($producto_id,$producto_nombre,$producto_precio)
    {
        Cart::add($producto_id,$producto_nombre,1,$producto_precio)->associate('App\Models\Producto');
        session()->flash('success_message','Producto agregado al carrito');
        return redirect()->route('producto.cart');
    }

    use WithPagination;
    public function render()
    {
        $categoria = Categoria::where('slug', $this->categoria_slug)->first();
        $categoria_id = $categoria->id;
        $categoria_nombre = $categoria->nombre;

        //Clasificación de los productos
        if ($this->clasificacion == 'date')
        {
            $productos = Producto::where('categoria_id', $categoria_id)->orderBy('created_at', 'DESC')->paginate($this->cantidad);
        }
        else if($this->clasificacion == 'price')
        {
            $productos = Producto::where('categoria_id', $categoria_id)->orderBy('precio_regular', 'ASC')->paginate($this->cantidad);
        }
        else if ($this->clasificacion == 'price-desc')
        {
            $productos = Producto::where('categoria_id', $categoria_id)->orderBy('precio_regular', 'DESC')->paginate($this->cantidad);
        }
        else
        {
            $productos = Producto::where('categoria_id', $categoria_id)->paginate($this->cantidad);
        }

        $categorias = Categoria::all();

        return view('livewire.category-component',['productos' => $productos, 'categorias' => $categorias, 'categoria_nombre'=>$categoria_nombre])->layout('layouts.base');
    }
}
