<main id="main">
    <div class="container">

        <!--MAIN SLIDE-->
        <div class="wrap-main-slide">
            <div class="slide-carousel owl-carousel style-nav-1" data-items="1" data-loop="1" data-nav="true" data-dots="false">
                @foreach ($sliders as $slider)
                    <div class="item-slide">
						<img src="{{ asset('assets/images/sliders') }}/{{ $slider->imagen }}" alt="" class="img-slide">
						<div class="slide-info slide-3">
							<h2 class="f-title"><b>{{ $slider->titulo }}</b></h2>
							<span class="f-subtitle">{{ $slider->subtitulo }}</span>
							<p class="sale-info">Precio único: <span class="price">${{ $slider->precio }}</span></p>
							<a href="{{ $slider->link }}" class="btn-link">Comprar Ahora</a>
						</div>
					</div>
                @endforeach
            </div>
        </div>

        <!--BANNER-->
        <div class="wrap-banner style-twin-default">
            <div class="banner-item">
                <a href="#" class="link-banner banner-effect-1">
                    <figure><img src="{{ asset('assets/images/home-1-banner-1.jpg') }}" alt="" width="580" height="190"></figure>
                </a>
            </div>
            <div class="banner-item">
                <a href="#" class="link-banner banner-effect-1">
                    <figure><img src="{{ asset('assets/images/home-1-banner-2.jpg') }}" alt="" width="580" height="190"></figure>
                </a>
            </div>
        </div>

        <!--Latest Products-->
        <div class="wrap-show-advance-info-box style-1">
            <h3 class="title-box">Productos recien agregados</h3>
            <div class="wrap-top-banner">
                <a href="#" class="link-banner banner-effect-2">
                    <figure><img src="{{ asset('assets/images/digital-electronic-banner.jpg') }}" width="1170" height="240" alt=""></figure>
                </a>
            </div>
            <div class="wrap-products">
                <div class="wrap-product-tab tab-style-1">						
                    <div class="tab-contents">
                        <div class="tab-content-item active" id="digital_1a">
                            <div class="wrap-products slide-carousel owl-carousel style-nav-1 equal-container" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"4"},"1200":{"items":"5"}}' >
                                @foreach ($ultimosProductos as $ultProducto)
                                    <div class="product product-style-2 equal-elem ">
                                        <div class="product-thumnail">
                                            <a href="{{ route('producto.detalles',['slug'=>$ultProducto->slug]) }}" title="{{ $ultProducto->nombre }}">
                                                <figure><img src="{{ asset('assets/images/products') }}/{{ $ultProducto->imagen }}" width="800" height="800" alt="{{ $ultProducto->nombre }}"></figure>
                                            </a>
                                            <div class="group-flash">
                                                <span class="flash-item new-label">new</span>
                                            </div>
                                            <div class="wrap-btn">
                                                <a href="{{ route('producto.detalles',['slug'=>$ultProducto->slug]) }}" class="function-link">vista rapida</a>
                                            </div>
                                        </div>
                                        <div class="product-info">
                                            <a href="{{ route('producto.detalles',['slug'=>$ultProducto->slug]) }}" class="product-name"><span>{{ $ultProducto->nombre }}</span></a>
                                            <div class="wrap-price"><span class="product-price">${{ $ultProducto->precio_regular }}</span></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>							
                    </div>
                </div>
            </div>
        </div>

        <!--Product Categories-->
        <div class="wrap-show-advance-info-box style-1">
            <h3 class="title-box">Produtos por Categoria</h3>
            <div class="wrap-top-banner">
                <a href="#" class="link-banner banner-effect-2">
                    <figure><img src="{{ asset('assets/images/fashion-accesories-banner.jpg') }}" width="1170" height="240" alt=""></figure>
                </a>
            </div>
            <div class="wrap-products">
                <div class="wrap-product-tab tab-style-1">
                    <div class="tab-control">
                        @foreach ($categorias as $key=>$categoria)
                            <a href="#categoria_{{ $categoria->id }}" class="tab-control-item {{ $key == 0 ? 'active':'' }}">{{ $categoria->nombre }}</a>
                        @endforeach
                    </div>
                    <div class="tab-contents">

                        @foreach ($categorias as $key=>$categoria)
                            <div class="tab-content-item {{ $key == 0 ? 'active':'' }}" id="categoria_{{ $categoria->id }}">
                                <div class="wrap-products slide-carousel owl-carousel style-nav-1 equal-container" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"4"},"1200":{"items":"5"}}' >
                                    @php
                                        $c_productos = DB::table('productos')->where('categoria_id',$categoria->id)->get()->take($no_productos);
                                    @endphp
                                    @foreach ($c_productos as $c_producto)
                                        <div class="product product-style-2 equal-elem ">
                                            <div class="product-thumnail">
                                                <a href="{{ route('producto.detalles',['slug'=>$c_producto->slug]) }}" title="{{ $c_producto->nombre }}">
                                                    <figure><img src="{{ asset('assets/images/products') }}/{{ $c_producto->imagen }}" width="800" height="800" alt="{{ $c_producto->nombre }}"></figure>
                                                </a>
                                                <div class="group-flash">
                                                    <span class="flash-item sale-label">sale</span>
                                                </div>
                                                <div class="wrap-btn">
                                                    <a href="{{ route('producto.detalles',['slug'=>$c_producto->slug]) }}" class="function-link">vista rapida</a>
                                                </div>
                                            </div>
                                            <div class="product-info">
                                                <a href="{{ route('producto.detalles',['slug'=>$c_producto->slug]) }}" class="product-name"><span>{{ $c_producto->nombre }}</span></a>
                                                <div class="wrap-price"><span class="product-price">${{ $c_producto->precio_regular }}</span></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>