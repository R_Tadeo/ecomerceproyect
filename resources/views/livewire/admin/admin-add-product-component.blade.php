<div>
    <div>
        <div class="container" style="padding: 30px 0px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6 text-uppercase">
                                    <h5>Nuevo Producto</h5> 
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.productos') }}" class="btn btn-success pull-right">Todos los Productos</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if (Session::has('message'))
                                <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                            @endif
                            <form class="form-horizontal container-form" enctype="multipart/form-data" wire:submit.prevent="guardarProducto">
                                <div class="mb-3 from-group">
                                    <label class="form-label">Nombre del Producto</label>
                                    <input type="text" placeholder="Nombre del Producto" class="form-control" wire:model="nombre" wire:keyup="generarSlug" required>
                                    @error('nombre')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Producto Slug</label>
                                    <input type="text" placeholder="Producto Slug" class="form-control" wire:model="slug" required>
                                    @error('slug')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt" wire:ignore>
                                    <label class="form-label">Descripción Corta</label>
                                    <textarea class="form-control" id="descripcion_corta" placeholder="Descripción Corta" rows="3" wire:model="descripcion_corta" required></textarea>
                                    @error('descripcion_corta')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt" wire:ignore>
                                    <label class="form-label">Descripción</label>
                                    <textarea class="form-control" id="descripcion" placeholder="Descripción" rows="3" wire:model="descripcion" required></textarea>
                                    @error('descripcion')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Precio</label>
                                    <input type="text" placeholder="Precio" class="form-control" wire:model="precio_regular" required>
                                    @error('precio_regular')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Precio en Oferta</label>
                                    <input type="text" placeholder="Precio en oferta" class="form-control" wire:model="precio_venta">
                                    @error('precio_venta')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">SKU</label>
                                    <input type="text" placeholder="SKU" class="form-control" wire:model="SKU" required>
                                    @error('SKU')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Existencia</label>
                                    <select class="form-select form-select-lg mb-3" aria-label="Default select example" wire:model="stock_estatus" required>
                                        <option value="instock">En existencia</option>
                                        <option value="agotado">Agotado</option>
                                    </select>
                                    @error('stock_estatus')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Destacado</label>
                                    <select class="form-select form-select-lg mb-3" aria-label="Default select example" wire:model="caracteristecas">
                                        <option value="0">No</option>
                                        <option value="1">Si</option>
                                    </select>
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Cantidad</label>
                                    <input type="text" placeholder="Cantidad" class="form-control" wire:model="cantidad" required>
                                    @error('cantidad')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Imagen del Producto</label>
                                    <input type="file"class="form-control" accept="image/png,image/jpeg,image/jpg" wire:model="imagen" required>
                                    @if ($imagen)
                                        <img src="{{ $imagen->temporaryUrl() }}" width="120px">
                                    @endif
                                    @error('imagen')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="mb-3 from-group mt">
                                    <label class="form-label">Categoria</label>
                                    <select class="form-select form-select-lg mb-3" aria-label="Default select example" wire:model="categoria_id" required>
                                        <option selected>Categoria</option>
                                        @foreach ($categorias as $categoria)
                                            <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                        @endforeach
                                        @error('categoria_id')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary mt">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .container-form{
            margin: 40px !important;
            padding: 0px 100px !important;
        }
    
        .mt{
            margin-top: 20px !important;
        }
    </style>
</div>

@push('scripts')
    <script>
        $(function(){
            tinymce.init({
                selector:'#descripcion_corta',
                setup:function(editor){
                    editor.on('Change',function(e){
                        tinyMCE.triggerSave();
                        var sd_data = $('#descripcion_corta').val();
                        @this.set('descripcion_corta',sd_data);
                    });
                }
            });

            tinymce.init({
                selector:'#descripcion',
                setup:function(editor){
                    editor.on('Change',function(e){
                        tinyMCE.triggerSave();
                        var sd_data = $('#descripcion').val();
                        @this.set('descripcion',sd_data);
                    });
                }
            });
        });
    </script>
@endpush