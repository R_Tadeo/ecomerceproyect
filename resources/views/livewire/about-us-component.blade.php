<div>
    <main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="/" class="link">home</a></li>
					<li class="item-link"><span>Nosotros</span></li>
				</ul>
			</div>
		</div>
		
		<div class="container">
			<!-- <div class="main-content-area"> -->
				<div class="aboutus-info style-center">
					<b class="box-title">Datos Interesantes</b>
					<p class="txt-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
				</div>

				<div class="row equal-container">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-box-score equal-elem ">
							<b class="box-score-title">La mejor</b>
							<span class="sub-title">Tenemos la mejor diseñadora</span>
							<p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-box-score equal-elem ">
							<b class="box-score-title">El guapo</b>
							<span class="sub-title">Sin duda el Ricarcas</span>
							<p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-box-score equal-elem ">
							<b class="box-score-title">Los poderosos</b>
							<span class="sub-title">El Pedro Y Tadeo, Como no.</span>
							<p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-info style-small-left">
							<b class="box-title">¿QUÉ HACEMOS REALMENTE?</b>
							<p class="txt-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum pharetra felis, at placerat justo efficitur vel. Aenean fringilla lacinia felis, quis interdum urna dictum non. </p>
						</div>
						<div class="aboutus-info style-small-left">
							<b class="box-title">Historia de la Compañía</b>
							<p class="txt-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum pharetra felis, at placerat justo efficitur vel. Aenean fringilla lacinia felis, quis interdum urna dictum non. </p>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-info style-small-left">
							<b class="box-title">NUESTRA VISIÓN</b>
							<p class="txt-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum pharetra felis, at placerat justo efficitur vel. Aenean fringilla lacinia felis, quis interdum urna dictum non. </p>
						</div>
						<div class="aboutus-info style-small-left">
							<b class="box-title">¡Cooperen con nosotros!</b>
							<p class="txt-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum pharetra felis, at placerat justo efficitur vel. Aenean fringilla lacinia felis, quis interdum urna dictum non. </p>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="aboutus-info style-small-left">
							<b class="box-title">¡Cooperen con nosotros!</b>
							<div class="list-showups">
								<label>
									<input type="radio" class="hidden" name="showup" id="shoup1" value="shoup1" checked="checked">
									<span class="check-box"></span>
									<span class='function-name'>Support 24/7</span>
									<span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
								</label>
								<label>
									<input type="radio" class="hidden" name="showup" id="shoup2" value="shoup2">
									<span class="check-box"></span>
									<span class='function-name'>Best Quanlity</span>
									<span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
								</label>
								<label>
									<input type="radio" class="hidden" name="showup" id="shoup3" value="shoup3">
									<span class="check-box"></span>
									<span class='function-name'>Fastest Delivery</span>
									<span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
								</label>
								<label>
									<input type="radio" class="hidden" name="showup" id="shoup4" value="shoup4">
									<span class="check-box"></span>
									<span class='function-name'>Customer Care</span>
									<span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="our-team-info">
					<h4 class="title-box">Team Frijolito</h4>
					<div class="our-staff">
						<div 
							class="slide-carousel owl-carousel style-nav-1 equal-container" 
							data-items="5" 
							data-loop="false" 
							data-nav="true" 
							data-dots="false"
							data-margin="30"
							data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"4"}}' >

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="LEONA">
										<figure><img src="assets/images/member-leona.jpg" alt="LEONA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">Campos Zepeda Heidy Noemy</b>
                                    <br>
									<span class="title">Desarrolladora - Diseñadora</span>
									<p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="LUCIA">
										<figure><img src="assets/images/member-lucia.jpg" alt="LUCIA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">cruz cruz ricardo antonio</b>
                                    <br>
									<span class="title">Desarrollador</span>
									<p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="NANA">
										<figure><img src="assets/images/member-nana.jpeg" alt="NANA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">López Lorenzo Pedro Emmanuel</b>
                                    <br>
									<span class="title">Desarrollador</span>
									<p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="BRAUM">
										<figure><img src="assets/images/member-braum.jpg" alt="BRAUM"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">Tadeo Mollinedo Ricardo</b>
                                    <br>
									<span class="title">Desarrollador</span>
									<p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="LUCIA">
										<figure><img src="assets/images/member-lucia.jpg" alt="LUCIA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">uc Salazar José Ángel</b>
                                    <br>
									<span class="title">Desarrollador</span>
									<p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
								</div>
							</div>

						</div>

					</div>

				</div>
			<!-- </div> -->
			

		</div><!--end container-->

	</main>
</div>
